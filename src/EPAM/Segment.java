package EPAM;


import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

    class Segment {
        Point a;
        Point b;
        Point c;
        double x1;
        double x2;
        double x3;
        double y1;
        double y2;
        double y3;
        double minx = 0;
        double miny = 0;



        public Segment(Point a, Point b, Point c) {

            this.a = a;
            this.b = b;
            this.c = c;

        }

        public double area() {

            x1 = Segment.this.a.getX();
            x2 = Segment.this.b.getX();
            x3 = Segment.this.c.getX();
            y1 = Segment.this.a.getY();
            y2 = Segment.this.b.getY();
            y3 = Segment.this.c.getY();

            double area = Math.abs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)));
            return area;
        }

        Point leftmostPoint() {
            x1 = Segment.this.a.getX();
            x2 = Segment.this.b.getX();
            x3 = Segment.this.c.getX();
            y1 = Segment.this.a.getY();
            y2 = Segment.this.b.getY();
            y3 = Segment.this.c.getY();

            if (x1 < x2) {
                if (x3 < x1) {
                    minx = x3;
                    miny = y3;
                    return
                } else {
                    minx = x1;
                    miny = y1;
                }
            } else {
                if (x2 < x3) {
                    minx = x2;
                    miny = y2;
                } else {
                    minx = x3;
                    miny = y3;
                }

            }
            Point p = new Point(minx, miny);
            return new Point(minx, miny);
        }
    }

