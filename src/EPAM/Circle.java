package EPAM;

public class Circle {
    Point a;
    double x1;
    double y1;
    double r;

    public Circle(Point a, double r) {
        this.a = a;
        this.r = r;
    }

    public double area() {

        x1 = Circle.this.a.getX();
        y1 = Circle.this.a.getY();

        double area = Math.PI * Math.pow(r,2);
        return area;
    }

}
