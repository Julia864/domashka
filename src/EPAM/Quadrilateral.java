package EPAM;

public class Quadrilateral {

    Point a;
    Point b;
    Point c;
    Point d;
    double x1;
    double x2;
    double x3;
    double x4;
    double y1;
    double y2;
    double y3;
    double y4;

    public Quadrilateral(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public double area() {

        x1 = Quadrilateral.this.a.getX();
        x2 = Quadrilateral.this.b.getX();
        x4 = Quadrilateral.this.c.getX();
        x3 = Quadrilateral.this.d.getX();
        y1 = Quadrilateral.this.a.getY();
        y2 = Quadrilateral.this.b.getY();
        y4 = Quadrilateral.this.c.getY();
        y3 = Quadrilateral.this.d.getY();

        double area1 = Math.abs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)));
        double area2 = Math.abs(0.5 * (x2 * (y4 - y3) + x4 * (y3 - y2) + x3 * (y2 - y4)));
        double area = area1 + area2;
        return area;
    }
}
